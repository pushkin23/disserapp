import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

import static spark.Spark.*;

public class Main {
    public static void main(String[] args) {
        port(8080);
        get("/rdy", (req, res) -> generateMessages());
        get("/sout", (req, res) -> "kek");
    }

    private static JSONObject generateMessages(){
        JSONObject obj = new JSONObject();
        try {
            String SPEC = "secp256k1";
            ECGenParameterSpec ecSpec = new ECGenParameterSpec(SPEC);
            String ALGO = "SHA256withECDSA";
            KeyPairGenerator g = null;
            g = KeyPairGenerator.getInstance("EC");
            g.initialize(ecSpec, new SecureRandom());
            KeyPair keypair = g.generateKeyPair();
            PublicKey publicKey = keypair.getPublic();
            PrivateKey privateKey = keypair.getPrivate();
            String plaintext = "Hello";
            Signature ecdsaSign = Signature.getInstance(ALGO);
            ecdsaSign.initSign(privateKey);
            ecdsaSign.update(plaintext.getBytes(StandardCharsets.UTF_8));
            byte[] signature = ecdsaSign.sign();
            String pub = Base64.getEncoder().encodeToString(publicKey.getEncoded());
            String sig = Base64.getEncoder().encodeToString(signature);
//            System.out.println(sig);
//            System.out.println(pub);
            obj.put("publicKey", pub);
            obj.put("signature", sig);
            obj.put("message", plaintext);
            obj.put("algorithm", ALGO);
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException | InvalidKeyException | SignatureException e) {
            e.printStackTrace();
            return null;
        }
        return obj;
    }

    private boolean receiver(JSONObject obj) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, UnsupportedEncodingException, SignatureException {

        Signature ecdsaVerify = Signature.getInstance(obj.getString("algorithm"));

        EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(obj.getString("publicKey")));

        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

        ecdsaVerify.initVerify(publicKey);
        ecdsaVerify.update(obj.getString("message").getBytes(StandardCharsets.UTF_8));
        boolean result = ecdsaVerify.verify(Base64.getDecoder().decode(obj.getString("signature")));

        return result;
    }
}
