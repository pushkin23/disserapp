import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import spark.Spark;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestClass {
    @BeforeEach
    void setUp() {
        Main.main(null);
    }

    @AfterEach
    void tearDown() throws InterruptedException {
        Thread.sleep(2000);
        Spark.stop();
    }

    @Test
    void testReceiving() throws IOException {
        String response = getStringResponse("sout");
        assertEquals("kek", response);
    }

    @Test
    void testGeneratingInfo() throws IOException {
        String response = getStringResponse("rdy");
        System.out.println(response);
        JSONObject object = new JSONObject(response);
        System.out.println(object.get("publicKey"));
    }

    private String getStringResponse(String route) throws IOException {
        CloseableHttpClient httpClient = HttpClients.custom()
                .build();
        String formatRoute = String.format("http://localhost:8080/%s", route);

        HttpGet httpGet = new HttpGet(formatRoute);
        CloseableHttpResponse response = httpClient.execute(httpGet);

        int statusCode = response.getStatusLine().getStatusCode();
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuilder result = new StringBuilder();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        if(statusCode == 200)
            return result.toString();
        return null;
    }
}
